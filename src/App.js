import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import AddProduct from "./components/AddProduct";
import ShowProduct from "./components/ShowProduct";
import EditProduct from "./components/EditProduct";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<ShowProduct />}></Route>
        <Route path="/add" element={<AddProduct />}></Route>
        <Route path="/edit/:id" element={<EditProduct />}></Route>
      </Routes>
    </Router>
  );
}

export default App;

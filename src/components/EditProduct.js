import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  editProduct,
  getProducts,
  productSelector,
  updateProduct,
} from "../features/Product/ProductSlice";

const EditProduct = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const { id } = useParams();
  const navigate = useNavigate();
  useEffect(() => {
    console.log("wwww");
    dispatch(editProduct(id));
    // dispatch(getProducts());
  }, [dispatch]);

  const data = useSelector((state) => productSelector.selectById(state, id));
  useEffect(() => {
    if (data) {
      setName(data.name);
      setPrice(data.price);
    }
  }, [data]);

  const doUpdate = async (e) => {
    e.preventDefault();
    await dispatch(updateProduct({ id, name, price }));
    navigate("/");
  };

  return (
    <div>
      {" "}
      <div>
        <h1>Edit Product</h1>
        <div>
          <form onSubmit={doUpdate}>
            <div>
              <label>Name</label>
              <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}></input>
            </div>
            <div>
              <label>Price</label>
              <input
                type="text"
                value={price}
                onChange={(e) => setPrice(e.target.value)}></input>
            </div>
            <button type="submit">save</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditProduct;

import React, { useState } from "react";

// untuk memasukan ke dalam store di redux kita harus mengimport ini dulu
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { saveProducts } from "../features/Product/ProductSlice";

// untuk mengubah slicenya import juga

const AddProduct = () => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const storeProduct = async (e) => {
    e.preventDefault();
    await dispatch(saveProducts({ name, price }));
    navigate("/");
  };

  return (
    <div>
      <h1>Add Product</h1>
      <div>
        <form onSubmit={storeProduct}>
          <div>
            <label>Name</label>
            <input
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}></input>
          </div>
          <div>
            <label>Price</label>
            <input
              type="text"
              value={price}
              onChange={(e) => setPrice(e.target.value)}></input>
          </div>
          <button type="submit">save</button>
        </form>
      </div>
    </div>
  );
};

export default AddProduct;

// untuk memanggil state yg sudah dibuat dapat mengimport menggunakan useSelector
// klo mau pake reduce harus di awali huruf besar

import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  getProducts,
  productSelector,
  deleteProducts,
} from "../features/Product/ProductSlice";
const ShowProduct = () => {
  //const { name, price } = useSelector((state) => state.Product); ini untuk mengambil data yg di simpan di store
  const dispatch = useDispatch();

  //useEffect(() => {dispatch(getProducts()}));
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);
  const data = useSelector(productSelector.selectAll);
  return (
    <div>
      <div>
        <Link to="add">Add</Link>
      </div>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((row, index) => (
            <tr key={row.id}>
              <td>{index + 1}</td>
              <td>{row.name}</td>
              <td>{row.price}</td>
              <td>
                <Link to={`edit/${row.id}`}>Edit</Link>
                <button onClick={() => dispatch(deleteProducts(row.id))}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ShowProduct;

// ini akan mengimport initialstate, reducer, dan akan membuat action creator dan action type secara otomatis
//createAsyncThunk fungsi ini sangat bermanfaat jika ingin bermain dengan async function ( untuk ambil data dari api)
//createEntityAdapter ini bermanfaat untuk digunakan dalam nested data

import axios from "axios";
import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";

// untuk megambil data dapat menggunakan ini

/*
    createAsyncThunk akan mengembalikan 3 value 
    products/get-product/pending -> jika masih di load datanya
    products/get-product/fulfilled -> jika berhasil
    products/get-product/rejected -> jika gagal
*/

export const getProducts = createAsyncThunk("products/get", async () => {
  const response = await axios.get("http://localhost:5000/products");
  return response.data;
});

export const editProduct = createAsyncThunk(
  "products/get-detail",
  async (id) => {
    const response = await axios.get(`http://localhost:5000/products/${id}`);
    return response.data;
  },
);

export const saveProducts = createAsyncThunk(
  "products/store",
  async ({ name, price }) => {
    const response = await axios.post("http://localhost:5000/products", {
      name,
      price,
    });
    return response.data;
  },
);

export const deleteProducts = createAsyncThunk(
  "products/delete",
  async (id) => {
    await axios.delete(`http://localhost:5000/products/${id}`);
    return id;
  },
);

export const updateProduct = createAsyncThunk(
  "products/get-detail",
  async ({ id, name, price }) => {
    const response = await axios.patch(`http://localhost:5000/products/${id}`, {
      name,
      price,
    });
    return response.data;
  },
);

const productEntity = createEntityAdapter({
  selectId: (product) => product.id,
});

// untuk mendeklarasi state apa aja yg bisa berubah
const productSlice = createSlice({
  name: "product",
  initialState: productEntity.getInitialState(),
  extraReducers: {
    [getProducts.fulfilled]: (state, action) => {
      productEntity.setAll(state, action.payload);
    },
    [saveProducts.fulfilled]: (state, action) => {
      productEntity.addOne(state, action.payload);
    },
    [editProduct.fulfilled]: (state, action) => {
      productEntity.setOne(state, action.payload);
    },
    [deleteProducts.fulfilled]: (state, action) => {
      productEntity.removeOne(state, action.payload);
    },
    [updateProduct.fulfilled]: (state, action) => {
      productEntity.updateOne(state, {
        id: action.payload.id,
        update: action.payload,
      });
    },
  },
});

/*reducers: {
    // untuk menambahkan fungsi yg dapat digunakan untuk mengubah nilai statenya
    update: (state, action) => {
      state.name = action.payload.name;
      state.price = action.payload.price;
    },
  },*/

export const productSelector = productEntity.getSelectors(
  (state) => state.Product, //"product" harus sama dengan yg ada di store
);

//export const { update } = productSlice.actions; // agar fungsinya bisa digunakan di tempat lainnya
export default productSlice.reducer; // agar dapat dipanggil di store;
